# Ariadne

A Minecraft kitchen-sink pack featuring numerous mods from Modrinth built on Quilt.

The modpack repository has a slightly unconventional setup: it is built for the [packwiz](https://packwiz.infra.link/) toolkit, but it also doubles as a fully functional vanilla Minecraft instance. So you have multiple installation options if you're comfortable with `git clone` and can run the `install-mods.sh` Unix shell script.

Most users will probably want to install this as a Modrinth pack via Prism Launcher or something along those lines.

**[Download from Modrinth](https://modrinth.com/modpack/ariadne)**

## TODO
* Better README
* custom mod for give feedback & report bugs?
* weird Mixin disabling in Charm
* Orechid integration
* Charm's Ebony wood?
