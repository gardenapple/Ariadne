// priority: 0

onEvent('item.registry', event => {
	event.create('adventure').displayName('Adventure Advancement Placeholder')
	event.create('decoration').displayName('Decoration Advancement Placeholder')
	event.create('equipment').displayName('Equipment Advancement Placeholder')
	event.create('food').displayName('Food Advancement Placeholder')
	event.create('magic').displayName('Magic Advancement Placeholder')
	event.create('storage').displayName('Storage Advancement Placeholder')
	event.create('technology').displayName('Technology Advancement Placeholder')
	event.create('utility').displayName('Utility Advancement Placeholder')
	event.create('world').displayName('World Advancement Placeholder')

	event.create('cobalt_dust').displayName('Cobalt Dust')
	event.create('crushed_cobalt_ore').displayName('Crushed Cobalt')
})
