const CRUSHABLE_ORES = {
	"stone": new Map([
		["tungsten", "createplus:crushed_tungsten_ore"],
		["antimony", "createplus:crushed_antimony_ore"],
		["adamantite", "createplus:crushed_adamantite_ore"],
		["aquarium", "createplus:crushed_aquarium_ore"],
		["banglum", "createplus:crushed_banglum_ore"],
		["carmot", "createplus:crushed_carmot_ore"],
		["kyber", "createplus:crushed_kyber_ore"],
		["mythril", "createplus:crushed_mythril_ore"],
		["orichalcum", "createplus:crushed_orichalcum_ore"],
		["prometheum", "createplus:crushed_prometheum_ore"],
		["runite", "createplus:crushed_runite_ore"],
		["iron", "create:crushed_iron_ore"],
		["gold", "create:crushed_gold_ore"],
		["copper", "create:crushed_copper_ore"],
		["zinc", "create:crushed_zinc_ore"],
		["osmium", "create:crushed_osmium_ore"],
		["lead", "create:crushed_lead_ore"],
		["silver", "create:crushed_silver_ore"],
		["tin", "create:crushed_tin_ore"],
		["uranium", "create:crushed_uranium_ore"],
		["nickel", "create:crushed_nickel_ore"],
		["coal", "minecraft:coal"],
		["diamond", "minecraft:diamond"],
		["emerald", "minecraft:emerald"],
		["soulstone", "malum:crushed_soulstone"],
		["lignite", "modern_industrialization:lignite_coal"],
		["mozanite", "modern_industrialization:mozanite_dust"],
		["salt", "modern_industrialization:salt_dust"],
		["bauxite", "modern_industrialization:bauxite_dust"],
		["unobtainium", "mythicmetals:unobtainium"],
		["starrite", "mythicmetals:starrite"],
		["morkite", "mythicmetals:morkite"],
		["certus_quartz", [
			Item.of("ae2:certus_quartz_crystal"), 
			Item.of("ae2:certus_quartz_crystal").withChance(0.75),
			Item.of("ae2:certus_quartz_dust", 2),
			Item.of("ae2:certus_quartz_dust").withChance(0.25)
		]],
		["gleaming", [
			Item.of("things:gleaming_powder"),
			Item.of("things:gleaming_powder").withChance(0.25),
			Item.of("create:experience_nugget").withChance(0.75)
		]],
		["lapis", [
			Item.of("minecraft:lapis_lazuli", 10),
			Item.of("minecraft:lapis_lazuli").withChance(0.5),
			Item.of("create:experience_nugget").withChance(0.75)
		]],
		["quantum", [
			Item.of("qcraft:quantum_dust", 3),
			Item.of("qcraft:quantum_dust").withChance(0.5),
			Item.of("create:experience_nugget").withChance(0.75)
		]],
		["redstone", [
			Item.of("minecraft:redstone", 6),
			Item.of("minecraft:redstone").withChance(0.5),
			Item.of("create:experience_nugget").withChance(0.75)
		]],
		["uraninite", [
			Item.of("powah:uraninite_raw", 3),
			Item.of("powah:uraninite_raw").withChance(0.5),
			Item.of("create:experience_nugget").withChance(0.75)
		]],
		["dragonstone", "horde-resource-crops:ores/raw_dragonstone_overworld"],
		["rhodonite", "horde-resource-crops:ores/raw_rhodonite"],
		["brookite", "yttr:brookite"],
		["gadolinite", [
			Item.of("yttr:yttrium_dust"),
			Item.of("yttr:yttrium_dust").withChance(0.75),
			Item.of("yttr:neodymium_dust").withChance(0.5),
			Item.of("create:experience_nugget").withChance(0.75)
		]]
	]),
	"nether": new Map([
		["banglum", "createplus:crushed_banglum_ore"],
		["midas_gold", "createplus:crushed_midas_gold_ore"],
		["palladium", "createplus:crushed_palladium_ore"],
		["stormyx", "createplus:crushed_stormyx_ore"],
		["emeraldite", "byg:emeraldite_shards"],
		["blazing_quartz", "malum:blazing_quartz"],
		["anthracite", [Item.of("byg:anthracite"), Item.of("byg:anthracite").withChance(0.75)]],
		["nether_gold", [
			Item.of("minecraft:gold_nugget", 18),
			Item.of("create:experience_nugget").withChance(0.75)
		]],
		["pendorite", [
			Item.of("byg:pendorite_scraps"),
			Item.of("byg:pendorite_scraps").withChance(0.25)
		]],
		["quartz", [
			Item.of("minecraft:quartz", 2),
			Item.of("minecraft:quartz").withChance(0.25),
			Item.of("create:experience_nugget").withChance(0.75)
		]],
		["dragonstone_nether", "horde-resource-crops:ores/raw_dragonstone_nether"],
		["rhodonite", "horde-resource-crops:ores/raw_rhodonite"],
		["cobalt", "kubejs:crushed_cobalt_ore"],
	]),
	"end": new Map([
		["tungsten", "createplus:crushed_tungsten_ore"],
		["starrite", "mythicmetals:starrite"],
		["lignite", "modern_industrialization:lignite_coal"],
		["ametrine", [Item.of("byg:ametrine_gems"), Item.of("byg:ametrine_gems").withChance(0.5)]],
		["dragonstone_end", "horde-resource-crops:ores/raw_dragonstone_end"],
		["rhodonite", "horde-resource-crops:ores/raw_rhodonite"]
	]),
}

const EXCAVATED_VARIANT_STONES = {
	"stone": new Map([
		["andesite", "minecraft:andesite"],
		["diorite", "minecraft:diorite"],
		["dripstone", "minecraft:dripstone"],
		["sandstone", "minecraft:sandstone"],
		["calcite", "minecraft:calcite"],
		["red_sandstone", "minecraft:red_sandstone"],
		["tuff", "minecraft:tuff"],
		["granite", "minecraft:granite"],
		["smooth_basalt", "minecraft:smooth_basalt"],
		["promenade_blunite", "promenade:blunite"],
		["promenade_carbonite", "promenade:carbonite"],
		["byg_dacite", "byg:dacite"],
		["byg_red_rock", "byg:red_rock"],
		["byg_mossy_stone", "byg:mossy_stone"],
		["byg_rocky", "byg:rocky_stone"],
		["byg_travertine", "byg:travertine"],
		["byg_soapstone", "byg:soapstone"],
		["twigs_rhyolite", "twigs:rhyolite"],
		["twigs_schist", "twigs:schist"],
		["create_asurine", "create:asurine"],
		["create_crimsite", "create:crimsite"],
		["create_limestone", "create:limestone"],
		["create_ochrum", "create:ochrum"],
		["create_scoria", "create:scoria"],
		["create_scorchia", "create:scorchia"],
		["create_veridium", "create:veridium"]
	]),
	"nether": new Map([
		["blackstone", "minecraft:blackstone"],
		["basalt", "minecraft:basalt"],
		["byg_scoria", "byg:scoria_stone"],
		["byg_brimstone", "byg:brimstone"],
		["byg_blue_netherrack", "byg:blue_netherrack"],
		["twigs_bloodstone", "twigs:bloodstone"],
		["create_smooth_basalt", "minecraft:smooth_basalt"],
		["create_scoria", "create:scoria"],
		["create_scorchia", "create:scorchia"]
	]),
	"end": new Map([
		["byg_purpur", "byg:purpur_stone"],
		["byg_ether_stone", "byg:ether_stone"],
		["byg_cryptic", "byg:cryptic_stone"]
	])
}

const TYPES = ["stone", "nether", "end"]

const WASHABLES = new Map([
	["kubejs:crushed_cobalt_ore", [
		Item.of("tconstruct:cobalt_nugget", 9), 
		Item.of("tconstruct:debris_nugget").withChance(0.25)
	]],
	["yttr:yttrium_dust", [
		Item.of("yttr:yttrium_nugget", 9), 
		Item.of("yttr:neodymium_dust").withChance(0.25)
	]]
])


function createWashing(event, outputs, input) {
	event.custom({
		type: 'create:splashing',
		ingredients: [
			Ingredient.of(input).toJson()
		],
		results: outputs.map(ingredient => ingredient.toResultJson())
	})
}

function createCrushing(event, outputs, input, processingTime) {
	event.custom({
		type: 'create:crushing',
		ingredients: [
			Ingredient.of(input).toJson()
		],
		results: outputs.map(ingredient => ingredient.toResultJson()),
		processingTime: processingTime
	})
}

function createCrushingForOre(event, input, ore_outputs, stone_output) {
	let outputs = ore_outputs
	if (typeof outputs === "string") {
		outputs = [
			Item.of(outputs),
			Item.of(outputs).withChance(0.75),
			Item.of("create:experience_nugget").withChance(0.75)
		]
	} else {
		outputs = Array.from(outputs)
	}
	outputs.push(Item.of(stone_output).withChance(0.25))
	createCrushing(event, outputs, input, 350)
}

onEvent('recipes', event => {
	for (let type of TYPES) {
		for (let [ore_id, ore_outputs] of CRUSHABLE_ORES[type]) {
			for (let [stone_id, stone_output] of EXCAVATED_VARIANT_STONES[type]) {
				createCrushingForOre(
					event, 
					`excavated_variants:${stone_id}_${ore_id}_ore`, 
					ore_outputs, 
					stone_output
				)
			}
		}
	}
	for (let [input, outputs] of WASHABLES) {
		createWashing(event, outputs, input)
	}

	event.smelting("tconstruct:cobalt_ingot", "kubejs:crushed_cobalt_ore").xp(0.1)
	event.blasting("tconstruct:cobalt_ingot", "kubejs:crushed_cobalt_ore").xp(0.1)
	event.smelting("tconstruct:cobalt_ingot", "kubejs:cobalt_dust")
	event.blasting("tconstruct:cobalt_ingot", "kubejs:cobalt_dust")
	event.remove({output: "#c:steel_ingots", input: "#c:iron_ingots", type: "minecraft:blasting"})
})
