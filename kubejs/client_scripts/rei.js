// priority: 0

const ORES = new Map([
	["adamantite", "Adamantite"],
	["ametrine", "Ametrine"],
	["anthracite", "Anthracite"],
	["antimony", "Antimony"],
	["aquarium", "Aquarium"],
	["banglum", "Banglum"],
	["bauxite", "Bauxite"],
	["blazing_quartz", "Blazing Quartz"],
	["brookite", "Brookite"],
	["carmot", "Carmot"],
	["certus_quartz", "Certus Quartz"],
	["coal", "Coal"],
	["cobalt", "Cobalt"],
	["copper", "Copper"],
	["diamond", "Diamond"],
	["dragonstone", "Dragonstone"],
	["dragonstone_end", "Nether Dragonstone"],
	["dragonstone_nether", "End Dragonstone"],
	["emerald", "Emerald"],
	["emeraldite", "Emeraldite"],
	["gadolinite", "Gadolinite"],
	["gleaming", "Gleaming"],
	["gold", "Gold"],
	["iron", "Iron"],
	["kyber", "Kyber"],
	["lapis", "Lapis"],
	["lead", "Lead"],
	["lignite", "Lignite"],
	["lignite", "Lignite"],
	["midas_gold", "Midas Gold"],
	["morkite", "Morkite"],
	["mozanite", "Mozanite"],
	["mythril", "Mythril"],
	["nether_gold", "Nether Gold"],
	["nickel", "Nickel"],
	["orichalcum", "Orichalcum"],
	["osmium", "Osmium"],
	["palladium", "Palladium"],
	["pendorite", "Pendorite"],
	["prometheum", "Prometheum"],
	["quantum", "Quantum"],
	["quartz", "Quartz"],
	["redstone", "Redstone"],
	["rhodonite", "Rhodonite"],
	["runite", "Runite"],
	["salt", "Salt"],
	["silver", "Silver"],
	["soulstone", "Soulstone"],
	["starrite", "Starrite"],
	["stormyx", "Stormyx"],
	["tin", "Tin"],
	["tungsten", "Tungsten"],
	["unobtainium", "Unobtainium"],
	["uraninite", "Uraninite"],
	["uranium", "Uranium"],
	["zinc", "Zinc"],
])

onEvent('rei.group', event => {
	event.groupSameItem("kubejs:rei_groups/ariadne/facade", "Facades", "simple_pipes:facade")
	for (const [ore, oreName] of ORES) {
		event.groupItemsByTag(`kubejs:rei_groups/ariadne/ores/${ore}`, `${oreName} Ores`, `c:${ore}_ores`)
	}
	event.groupItemsByTag("kubejs:rei_groups/ariadne/ores/uraninite_poor", "Poor Uraninite Ores", "c:uraninite_ore_poors")
	event.groupItemsByTag("kubejs:rei_groups/ariadne/ores/uraninite_dense", "Dense Uraninite Ores", "c:uraninite_ore_denses")
	event.groupItems("kubejs:rei_groups/ariadne/slopes", "Slopes", [ /^automobility:.*_slope$/ ])
	event.groupSameItem("kubejs:rei_groups/ariadne/crafting_stations", "Crafting Stations", "tconstruct:crafting_station")
	event.groupSameItem("kubejs:rei_groups/ariadne/tinker_stations", "Tinker Stations", "tconstruct:tinker_station")
	event.groupSameItem("kubejs:rei_groups/ariadne/part_builders", "Part Builders", "tconstruct:part_builder")
})
onEvent('rei.remove.categories', event => {
	console.log(event.getCategoryIds())
	event.yeet("minecraft:plugins/tag")
})
