execute as @a[scores={rejoined_game=1..}] run tellraw @s { "translate": "misc.ariadne.tip.emi_rei.1"}
execute as @a[scores={rejoined_game=1..}] run tellraw @s { "translate": "misc.ariadne.tip.emi_rei.2"}
execute as @a[scores={rejoined_game=1..}] run tellraw @s { "translate": "misc.ariadne.tip.emi_rei.3"}
execute as @a[scores={rejoined_game=1..}] run tellraw @s { "translate": "misc.ariadne.tip.emi_rei.4"}
execute as @a[tag=!JoinedOnce] run tellraw @s { "translate": "misc.ariadne.tip.emi_rei.1" }
execute as @a[tag=!JoinedOnce] run tellraw @s { "translate": "misc.ariadne.tip.emi_rei.2" }
execute as @a[tag=!JoinedOnce] run tellraw @s { "translate": "misc.ariadne.tip.emi_rei.3" }
execute as @a[tag=!JoinedOnce] run tellraw @s { "translate": "misc.ariadne.tip.emi_rei.4" }
execute as @a[scores={rejoined_game=1..}] run scoreboard players reset @s rejoined_game
execute as @a[tag=!JoinedOnce] run tag @s add JoinedOnce
